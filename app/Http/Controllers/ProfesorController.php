<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class ProfesorController extends Controller
{
    public function __construct()
    {
        //indica que acceder a las vistas y recursos de este controlador necesitan autenticacion
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profesoresList = App\Profesor::paginate(5);
        return view('profesores.profesores', compact('profesoresList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('profesores.profesores_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->all();
        $newProfesor = new App\Profesor;
        $newProfesor->name = $request -> name;
        $newProfesor->celphone = $request -> celphone;
        $newProfesor->save();
        return redirect()->route('profesor.index')->with('mensaje','Nuevo profesor creado!');
        //return redirect()->back();//->with('mensaje','Nuevo profesor creado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = App\Profesor::findOrFail($id);
        return view('profesores.profesores_edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesor = App\Profesor::findOrFail($id);
        $profesor->name = $request->name;
        $profesor->celphone = $request->celphone;
        $profesor->save();
        return redirect()->route('profesor.index')->with('mensaje','Profesor editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profesor = App\Profesor::findOrFail($id);
        $profesor->delete();
        return redirect()->route('profesor.index')->with('mensaje','Profesor eliminado!');
    }
}
