@extends("plantilla")

@section("seccion")
    <h1>Editar profesor</h1>
<form action="{{ route('profesor.update', $item->id) }}" method="POST">
        @csrf
        @method('PUT')
        <input type="text" name="name" placeholder="Nombre" class="form-control mb-4" value="{{ $item->name }}">
        <input type="text" name="celphone" placeholder="Telefono" class="form-control mb-4" value="{{ $item-> celphone}}">
        <button class="btn btn-primary btn-block" type="submit">Editar</button>
    </form>
@endsection