<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class EstudianteController extends Controller
{

    public function __construct()
    {
        //indica que acceder a las vistas y recursos de este controlador necesitan autenticacion
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estudiantesList = App\Estudiante::paginate(4);
        return view('estudiantes.estudiantes', compact('estudiantesList'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profesorList = App\Profesor::all();
        return view('estudiantes.estudiantes_add', compact('profesorList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $estudiante = new App\Estudiante();
        $estudiante->name = $request->name;
        $estudiante->direccion = $request->direccion;
        $profesorId = $request->input('profesores');
        $estudiante->profesor_id = $profesorId;
        $estudiante->save();
        return redirect()->route('estudiantes.index')->with('mensaje','Estudiante Agregado!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = App\Estudiante::findOrFail($id);
        $profesorList = App\Profesor::all();
        return view('estudiantes.estudiantes_edit', compact('item', 'profesorList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $estudiante = App\Estudiante::findOrFail($id);
        $estudiante->name = $request->name;
        $estudiante->direccion = $request->direccion;
        $profesorId = $request->input('profesores');
        $estudiante->profesor_id = $profesorId;
        $estudiante->save();
        return redirect()->route('estudiantes.index')->with('mensaje','Estudiante editado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estudiante = App\Estudiante::findOrFail($id);
        $estudiante->delete();
        return redirect()->route('estudiantes.index')->with('mensaje','Estudiante eliminado!');
    }

    /**
     * Retorna los estudiantes asignados al profesor
     * **/
    public function getByProfesor($idProfesor){
        $profesor = App\Profesor::find($idProfesor);
        $estudiantesList = $profesor->estudiantes;
        return view('estudiantes.estudiantes_by_profesor', compact('estudiantesList', 'profesor'));
    }
}
