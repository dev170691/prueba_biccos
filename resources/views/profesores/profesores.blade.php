@extends("plantilla")

@section("seccion")
    <h1>Profesores</h1>
    @if ( session('mensaje') )
        <div class="alert alert-success">{{ session('mensaje') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button></div>
    @endif
    <a href="{{ route('profesor.create') }}">
        <button class="btn btn-primary mb-4">Agregar</button>  
    </a>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">#Id</th>
        <th scope="col">Nombre</th>
        <th scope="col">Telefono</th>
        <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($profesoresList as $item)
        <tr>
        <th scope="row">{{ $item -> id}}</th>
        <td>{{$item -> name}}</td>
        <td>{{$item -> celphone}}</td>
        <td>
            <a href="{{ route('profesor.edit', $item) }}"><button class="btn d-inline">Editar</button></a>
            <a href="{{ route('estudiantesByProfesor', $item) }}"><button class="btn d-inline">List estudiantes</button></a>
            <form action="{{ route('profesor.destroy', $item) }}" class="d-inline" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
            </form> 
        </td>
        </tr>
    @endforeach
    </tbody>
    </table>
    {{ $profesoresList->links() }}
@endsection