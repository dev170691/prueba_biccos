@extends("plantilla")

@section("seccion")
    <h1>Crear profesor</h1>
<form action="{{ route('profesor.store') }}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Nombre" class="form-control mb-4">
        <input type="text" name="celphone" placeholder="Telefono" class="form-control mb-4">
        <button class="btn btn-primary btn-block" type="submit">Agregar</button>
    </form>
@endsection