<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesor extends Model
{
    //indica a traves de esta property que un profesor puede tener varios estudiantes asignados
    public function estudiantes()
    {
        return $this->hasMany('App\Estudiante');
    }
}
