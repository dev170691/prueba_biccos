@extends("plantilla")

@section("seccion")
    <h1>Crear estudiante</h1>
<form action="{{ route('estudiantes.store') }}" method="POST">
        @csrf
        <input type="text" name="name" placeholder="Nombre" class="form-control mb-4">
        <input type="text" name="direccion" placeholder="Direccion" class="form-control mb-4">
        <label for="profesores">Profesor:</label>
        <select name="profesores" id="profesores" class="form-control mb-4">
            @foreach($profesorList as $profesor )
              <option value="{{ $profesor->id }}">{{ $profesor->name }}</option>
            @endforeach
          </select>
        <button class="btn btn-primary btn-block" type="submit">Agregar</button>
    </form>
@endsection