<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
   // return view('welcome');
    return view('profesores');
}) -> name('profesores');*/

Route::resource('profesor', "ProfesorController");

Route::get('estudiantes/byProfesor/{idProfesor}', 'EstudianteController@getByProfesor')->name('estudiantesByProfesor');
Route::resource('estudiantes', 'EstudianteController');

/*Route::get('/estudiantes', function () {
     return view('estudiantes');
 }) -> name('estudiantes');*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
