@extends("plantilla")

@section("seccion")
    <h2>Estudiantes asignados a:</h2>
    <h1 class="mb-4">{{$profesor->name}}</h1>
    @if ( session('mensaje') )
        <div class="alert alert-success">{{ session('mensaje') }}
        <button type="button" class="close" data-dismiss="alert">&times;</button></div>
    @endif
    <table class="table">
    <thead>
        <tr>
        <th scope="col">#Id</th>
        <th scope="col">Nombre</th>
        <th scope="col">Direccion</th>
        <th scope="col">Pofesor</th>
        <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
    @foreach ($estudiantesList as $item)
        <tr>
        <th scope="row">{{ $item -> id}}</th>
        <td>{{$item -> name}}</td>
        <td>{{$item -> direccion}}</td>
        <td>{{$item -> profesor->name}}</td>
        <td>
            <a href="{{ route('estudiantes.edit', $item) }}"><button class="btn d-inline">Editar</button></a>
            <form action="{{ route('estudiantes.destroy', $item) }}" class="d-inline" method="POST">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
            </form> 
        </td>
        </tr>
    @endforeach
    </tbody>
    </table>
@endsection