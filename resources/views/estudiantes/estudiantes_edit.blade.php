@extends("plantilla")

@section("seccion")
    <h1>Editar estudiante</h1>
<form action="{{ route('estudiantes.update', $item->id) }}" method="POST">
        @csrf
        @method('PUT')
        <input type="text" name="name" placeholder="Nombre" class="form-control mb-4" value="{{ $item->name}}">
        <input type="text" name="direccion" placeholder="Direccion" class="form-control mb-4" value="{{ $item->direccion}}">
        <select name="profesores" id="profesores" class="form-control mb-4">
            @foreach($profesorList as $profesor )
              <option value="{{ $profesor->id }}"
                {{ ($profesor->id === $item->profesor_id) ? 'selected' : '' }}>{{ $profesor->name }}</option>
            @endforeach
          </select>
        <button class="btn btn-primary btn-block" type="submit">Editar</button>
    </form>
@endsection