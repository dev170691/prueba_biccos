<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    //Profesor referenciado por el id_profesor
    public function profesor()
    {
        return $this->belongsTo('App\Profesor');
    }
}
